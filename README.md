###Repository includes:

- All Java source code, with an Eclipse .project file
- Datasets for the OLA problem (small "toy problem" example, as well as examples of larger, more complex grids)
- Final Word doc report summarizing the results of the genetic algorithm project