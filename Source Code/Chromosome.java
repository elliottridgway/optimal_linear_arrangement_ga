import java.util.Random;
import java.util.ArrayList;

public class Chromosome implements Comparable<Chromosome> {
	private ArrayList <Integer> alleleList;
	private int fitness;
	private double inverseFitness;
	public double wheelPercentage;
	
	public Chromosome (int chromSize) {
		alleleList = new ArrayList<Integer>();
		
		if (chromSize >= 0) {
			Random alleleGenerator = new Random();
			int position;
			ArrayList<Integer> initialList = new ArrayList<Integer>();
		
			for (int i = 0; i < chromSize; i++) {
				initialList.add (i, i+1);
			}
		
			for (int i = 1; i <= chromSize; i++) {
				position = alleleGenerator.nextInt(initialList.size());
				alleleList.add(initialList.remove(position));
			}
			fitness = 0;
		}
		else {
			for (int i = 0; i < Math.abs(chromSize); i++) {
				alleleList.add(0);
			}
			fitness = 0;
		}
	}
	
	public Chromosome () {
		alleleList = new ArrayList<Integer>();
	}
	
	public Chromosome (ArrayList<Integer> list) {
		alleleList = list;
	}
	
	public void add (int arg0, int arg1) {
		alleleList.add(arg0, arg1);
	}
	
	public void add (int arg0) {
		alleleList.add(arg0);
	}
	
	public int remove(int arg0) {
		return alleleList.remove(arg0);
	}
	
	public int removeObject (int value) {
		for (int i = 0; i < size(); i++) {
			if (alleleList.get(i) == value) {
				alleleList.remove(i);
				return 0;
			}
		}
		return -1;
	}
	
	public int findObjectLoc (int value) {
		for (int i = 0; i < size(); i++) {
			if (alleleList.get(i) == value) {
				return i;
			}
		}
		return -1;
	}
	
	public int getAllele (int i) {
		return alleleList.get(i);
	}
	
	public void setAllele (int i, int value) {
		alleleList.set(i, value);
	}
	
	public void setFitness (int f) {
		fitness = f;
	}
	
	public int getFitness () {
		return fitness;
	}
	
	public void setInverseFitness (double f) {
		inverseFitness = f;
	}
	
	public double getInverseFitness () {
		return inverseFitness;
	}
	
	public int compareTo (Chromosome c) {
		return fitness - c.getFitness();
	}

	public int size () {
		return alleleList.size();
	}
	
	public void copy (Chromosome c) {
		for (int i = 0; i < c.size(); i++) {
			alleleList.add(c.getAllele(i));
		}
	}
	
	//Print the contents of the chromosome.
	public void print () {
		for (int i = 0; i < alleleList.size(); i++) {
			System.out.print (alleleList.get(i) + " ");
		}
		System.out.println();
	}
}
