import java.util.Scanner;
import java.util.Random;
import java.util.Collections;
import java.util.ArrayList;
import java.io.*;

/*
 * Genetic Algorithm, Simulated Annealing, and Foolish-Hill Climbing Algorithms
 * to Solve the Optimal Linear Arrangement Problem
 * written by Elliott Ridgway
 */

public class GeneticAlgorithm {
	private int algorithmChoice = 0;
	
	private double crossoverRate, mutationRate;
	private int populationSize, size;				//populationSize = size of the population
													//size = length of the dataset
	private int height, width;						//height and width of the 2x2 PPP board.
	
	private int selectionMethod = 0;				//1 = Roulette, 2 = Rank
	private int crossoverMethod = 0;				//1 = Order 1, 2 = Order 2 
	private int mutationOperator = 0;				//1 = Single Move, 2 = Pairwise Exchange
	private int datasetLoader = 0;					//1 = Load a file, 2 = Generate randomly
	
	private int eliteFitness = 0;
	private Chromosome eliteChromosome;
	
	private ArrayList<Chromosome> population, newPopulation;
	private int[][] dataset;
	
	public GeneticAlgorithm () {
		double tempValue;
		Scanner getInput = new Scanner (System.in);
		crossoverRate = -1;
		do {
			System.out.println ("Please which algorithm you want to run.");
			System.out.println ("(1 = Genetic Algorithm)");
			System.out.println ("(2 = Simulated Annealing)");
			System.out.println ("(3 = Foolish Hill-Climbing)");
			tempValue = getInput.nextInt();
			if (tempValue > 0 || tempValue < 4) algorithmChoice = (int)tempValue;
		} while (tempValue < 0 || tempValue > 4);
		if (algorithmChoice == 1) {
			do {
				System.out.print ("Please specify the crossover rate (between 0 and 100): ");
				tempValue = getInput.nextDouble();
				if (tempValue > 0 && tempValue <= 100) crossoverRate = tempValue;
			} while (crossoverRate == -1);
			mutationRate = -1;
			do {
				System.out.print ("Please specify the mutation rate (between 0 and 100): ");
				tempValue = getInput.nextDouble();
				if (tempValue > 0 && tempValue <= 100) mutationRate = tempValue;
			} while (mutationRate == -1);
			populationSize = -1;
			do {
				System.out.print ("Please specify the population size (must be an even number): ");
				tempValue = getInput.nextDouble();
				if (tempValue > 0) populationSize = (int)tempValue;
			} while (populationSize == -1 || populationSize%2 != 0);
			do {
				System.out.print ("Please specify the selection method (1 for roulette, 2 for rank): ");
				tempValue = getInput.nextInt();
				if (tempValue > 0 && tempValue < 3)  selectionMethod = (int)tempValue;
			} while (selectionMethod == 0);
			do {
				System.out.print ("Please specify the crossover method (1 for Order 1, 2 for Order 2): ");
				tempValue = getInput.nextInt();
				if (tempValue > 0 && tempValue < 3)  crossoverMethod = (int)tempValue;
			} while (crossoverMethod == 0);
		}
		else mutationRate = 100;
		do {
			System.out.print ("Please specify the mutation operator (1 for pairwise exchange, 2 for cycle of 3): ");
			tempValue = getInput.nextInt();
			if (tempValue > 0 && tempValue < 3)  mutationOperator = (int)tempValue;
		} while (mutationOperator == 0);
		do {
			System.out.print ("Do you want to load a dataset (1) or generate one randomly? (2)");
			tempValue = getInput.nextInt();
			if (tempValue > 0 && tempValue < 3)  datasetLoader = (int)tempValue;
		} while (datasetLoader == 0);
		
		population = new ArrayList<Chromosome>();
		
		if (algorithmChoice == 1) runGA();
		else if (algorithmChoice == 2) runSA(false);
		else if (algorithmChoice == 3) runSA(true);
	}
	
	
	long GACurrTime, GAEndTime;
	int GAIterations = 0;
	double variance = 0.0;
	double stDev = 0.0;
	
	public boolean timeToStop () {
		if (GAIterations == 0) {
			GAEndTime = System.nanoTime() / 1000000000L;
			GAEndTime += 180;
		}
		
		//Find the variance of the population.
		int mean = getMean();
		int temp = 0;
		for (int i = 0; i < population.size(); i++) {
			temp += (population.get(i).getFitness() - mean)*(population.get(i).getFitness() + mean);
		}
		variance = temp/population.size();
		stDev = Math.sqrt(variance);
		
		System.out.println ("Mean is " + mean + ", st. dev. is " + stDev);	
		
		GAIterations++;
		GACurrTime = System.nanoTime() / 1000000000L;
		
		//int timeLeft = (int)(GAEndTime - GACurrTime);
		//System.out.println ("Time Left:  " + timeLeft);
		
		if (GAIterations > 1 && (GAIterations == 30000 || GACurrTime >= GAEndTime)) return true;
		else return false;
	}
	
	
	public int getMean () {
		int sum = 0;
		for (int i = 0; i < population.size(); i++) {
			sum += population.get(i).getFitness();
		}
		sum /= population.size();
		return sum;
	}
	
	public void runGA() {
		//Read in the dataset.
		if (datasetLoader == 1) {
			System.out.println("Reading the dataset for Package Placement Problem...");
			try {
				File dataFile = new File("dataset.txt");
				Scanner fileReader = new Scanner(dataFile);
				
				width = fileReader.nextInt();				//First number in the file is the width (row-length) of the 2x2 array.
				height = fileReader.nextInt();				//Second number is the height (column-height)
				
				size = width*height;
				
				System.out.println ("Width is " + width + ", height is " + height);
				
				dataset = new int[size][size];
				
				//Now read the data into a 2x2 array.
				int thisNumber;
				for (int i = 0; i < size; i++){
					for (int j = i+1; j < size; j++) {
						thisNumber = fileReader.nextInt();
						dataset[i][j] = thisNumber;
						dataset[j][i] = thisNumber;
					}
				}
				
				for (int i = 0; i < size; i++){
					for (int j = 0; j < size; j++) {
						System.out.print (dataset[i][j] + " \t");
					}
					System.out.println();
				}
			}
			catch (IOException e) {
				System.out.println ("Could not find file.");
			}
		}
		else {
			System.out.println("Generating a random dataset for Package Placement Problem...");
			Random generator = new Random ();
			
			width = 2;  height = 30;
			size = width * height;
			System.out.println ("Width is " + width + ", height is " + height);
			
			dataset = new int[size][size];
			
			//Now read the data into a 2x2 array.
			int thisNumber;
			for (int i = 0; i < size; i++){
				for (int j = i+1; j < size; j++) {
					thisNumber = generator.nextInt(21);		//Random number between 0 and 20.
					dataset[i][j] = thisNumber;
					dataset[j][i] = thisNumber;
				}
			}
			
			for (int i = 0; i < size; i++){
				for (int j = 0; j < size; j++) {
					System.out.print (dataset[i][j] + " \t");
				}
				System.out.println();
			}
			
			//Save the randomly generated dataset in a file.
			File newDataset = new File ("randomDataset.txt");
			BufferedOutputStream bufferedOutput = null;
			String intString = null;
			try {
	            bufferedOutput = new BufferedOutputStream(new FileOutputStream(newDataset));
	            intString = new String(width + " " + height);
	            bufferedOutput.write(intString.getBytes());
	            bufferedOutput.write(" \r\n\r\n".getBytes());
				for (int i = 0; i < size; i++){
					for (int j = i+1; j < size; j++) {
						intString = new String (dataset[i][j] + " ");
						bufferedOutput.write (intString.getBytes());
					}
					bufferedOutput.write("\r\n".getBytes());
				}
				bufferedOutput.flush();
				bufferedOutput.close();
			}
			catch (IOException e) {
				System.out.println ("IO Exception: " + e);
			}
		}
		
		
		//Create the population of chromosomes, based on the size given by the user.
		for (int i = 0; i < populationSize; i++) {
			population.add(new Chromosome(size));
		}
		
		/*
		System.out.println ("Printing the chromosomes...");
		for (int i = 0; i < populationSize; i++) {
			population.get(i).print();
		}
		*/
		
		//Here's the main body of the algorithm - it checks the fitnesses, and
		//performs reproduction, crossover, and mutation.
		
		while (! timeToStop()) {
			System.out.println ("Currently on cycle " + GAIterations);
			//System.out.println ("Get the fitnesses...");
			
			//Run the fitness function on every chromosome and check for elitism.
			for (int i = 0; i < populationSize; i++) {
				int thisFitness = fitnessFunction (population.get(i));
				population.get(i).setFitness(thisFitness);
				if (thisFitness < eliteFitness || eliteFitness == 0) {
					eliteFitness = thisFitness;
					eliteChromosome = population.get(i);
				}
			}
			//System.out.println ("done!");
			//System.out.print ("Perform reproduction...");
			
			//Stage 1:  Reproduce the population for the parent pool.
			newPopulation = reproduction(population, selectionMethod);
			//System.out.println ("done!  New population is " + newPopulation.size());
			
			/*
			for (int i = 0; i < populationSize; i++) {
				newPopulation.get(i).print();
			}
			System.out.println ("Perform crossover...");
			*/
			
			//Stage 2:  Perform crossover randomly on the parent pool.
			Random parent1 = new Random();
			Random parent2 = new Random();
			Random rateCheck = new Random();
			int parent1Val, parent2Val;
			ArrayList<Chromosome> childPool = new ArrayList<Chromosome>();
			
			int steps = 0;
			while (steps < newPopulation.size()) {			
				parent1Val = parent1.nextInt(newPopulation.size());
				do {
					parent2Val = parent2.nextInt(newPopulation.size());
				} while (parent2Val == parent1Val);
				
				double willCrossoverHappen = rateCheck.nextDouble();
				if (willCrossoverHappen >= (crossoverRate/100.0)) {
					childPool.add(newPopulation.get(parent1Val));
					childPool.add(newPopulation.get(parent2Val));
				}
				else {
					childPool.add(crossover(newPopulation.get(parent1Val), newPopulation.get(parent2Val), crossoverMethod));
					childPool.add(crossover(newPopulation.get(parent2Val), newPopulation.get(parent1Val), crossoverMethod));
				}
				steps+=2;
			}
			//System.out.println ("done!  Child pool is " + childPool.size() + ", parent population is " + population.size());
			
			//Run the fitness function on every chromosome in the new population and check for elitism.
			for (int i = 0; i < populationSize; i++) {
				int thisFitness = fitnessFunction (childPool.get(i));
				childPool.get(i).setFitness(thisFitness);		
				//System.out.println(newPopulation.get(i).getFitness());
				if (thisFitness < eliteFitness || eliteFitness == 0) {
					eliteFitness = thisFitness;
					eliteChromosome = childPool.get(i);
				}
			}
			newPopulation = childPool;
			
			//System.out.println ("Perform mutation...");
			//Stage 3:  Perform mutation.
			for (int i = 0; i < newPopulation.size(); i++) {
				double willMutationHappen = rateCheck.nextDouble();
				parent1Val = parent1.nextInt(newPopulation.size());
				if (willMutationHappen >= (mutationRate/100.0)) continue;
				
				mutation(childPool.get(parent1Val), mutationOperator);
			}
			//System.out.println ("done!");
			
			//Run the fitness function on every chromosome in the new population and check for elitism.
			for (int i = 0; i < populationSize; i++) {
				int thisFitness = fitnessFunction (newPopulation.get(i));
				newPopulation.get(i).setFitness(thisFitness);		
				//System.out.println(newPopulation.get(i).getFitness());
				if (thisFitness < eliteFitness || eliteFitness == 0) {
					eliteFitness = thisFitness;
					eliteChromosome = newPopulation.get(i);
				}
			}
			
			//Finally, replace the old population with the new one.
			population = newPopulation;
			System.out.println ("Fittest so far: " + eliteFitness + "!");
			eliteChromosome.print();
		}
		
		System.out.println ("And the final answer is " + eliteFitness + "!");
		eliteChromosome.print();
	}
	
	//This is the simulated annealing algorithm.
	//Note:  This method also doubles as the foolish hill-climbing algorithm.
	//If hillClimbing parameter = true, then do hill-climbing.
	//Otherwise, do simulated annealing.
	double temperature;
	double saIterations;
	Chromosome solution = new Chromosome();
	int perturbationNumber = 0;
	public void runSA (boolean hillClimbing) {
		//Read in the dataset.
		System.out.println("Reading the dataset for Package Placement Problem...");
		try {
			File dataFile = new File("dataset.txt");
			Scanner fileReader = new Scanner(dataFile);
			
			width = fileReader.nextInt();				//First number in the file is the width (row-length) of the 2x2 array.
			height = fileReader.nextInt();				//Second number is the height (column-height)
			
			size = width*height;
			
			System.out.println ("Width is " + width + ", height is " + height);
			
			dataset = new int[size][size];
			
			//Now read the data into a 2x2 array.
			int thisNumber;
			for (int i = 0; i < size; i++){
				for (int j = i+1; j < size; j++) {
					thisNumber = fileReader.nextInt();
					dataset[i][j] = thisNumber;
					dataset[j][i] = thisNumber;
				}
			}
			
			for (int i = 0; i < size; i++){
				for (int j = 0; j < size; j++) {
					System.out.print (dataset[i][j] + " \t");
				}
				System.out.println();
			}
		}
		catch (IOException e) {
			System.out.println ("Could not find file.");
		}
		
		//This is the main body of the simulated annealing algorithm.
		solution = new Chromosome(size);
		solution.setFitness(fitnessFunction(solution));
		
		System.out.println ("Chromsome fitness is " + solution.getFitness());
		solution.print();
		
		temperature = 20;
		saIterations = 10000;
		int innerLoop = 0;
		
		long time = System.nanoTime() / 1000000000L;
		long finishingTime = time + 45L;
		Random randomSA = new Random();
		
		while (time < finishingTime) {
			while (innerLoop < saIterations) {
				//NewS := perturb(S)
				Chromosome newSolution = new Chromosome();
				newSolution.copy(solution);
				mutation (newSolution, mutationOperator);
				perturbationNumber++;
				
				int NewS = fitnessFunction(newSolution);
				int OldS = fitnessFunction(solution);
				if (hillClimbing == false) {
					if (NewS < OldS || randomSA.nextDouble() < Math.exp((OldS-NewS)/temperature)) {
						solution = newSolution;
						solution.setFitness(NewS);
					}
				}
				else {
					if (NewS < OldS) {
						solution = newSolution;
						solution.setFitness(NewS);
					}
				}
				innerLoop++;
			}
			System.out.println ("Out of inner loop.  Answer so far: " + solution.getFitness() + " Temp: " + temperature + " Iterations: " + saIterations);
			temperature = 0.95*temperature;
			saIterations = 1.05*saIterations;
			time = System.nanoTime() / 1000000000L;
			innerLoop = 0;
		}
		
		System.out.println ("Done!  And the answer is " + solution.getFitness());
		System.out.println ("Number of perturbations: " + perturbationNumber);
		solution.print();
	}
	
	public int fitnessFunction (Chromosome c) {
		int fitness = 0, difference = 0;
		//System.out.println ("The chromosome: "); c.print();
		for (int start = 0; start < size-1; start++) {
			for (int end = start+1; end < size; end++) {
				//Locate the start and end points on the 2D board.
				//First, find out what row the start and end points are on.
				int startRow = (start/width);				
				int endRow = (end/width);
				
				//Now, calculate the vertical distance.
				int verticalDifference = Math.abs(endRow - startRow);
				
				//Now, find the horizontal distance.	
				int horizontalDifference = Math.abs((end%width) - (start%width));
				
				//Now use the horizontal distance + vertical distance as the total distance.
				difference = horizontalDifference + verticalDifference;
				
				//System.out.print ("Horiz: " + horizontalDifference + ", Vert: " + verticalDifference + ", Distance: " + difference + ".  ");
				fitness += (difference * dataset[c.getAllele(start)-1][c.getAllele(end)-1]);
				//System.out.println (c.getAllele(start) + ", " + c.getAllele(end) + ", Fitness so far:  " + fitness);
			}
		}
		fitness = fitness - 162700;
		//System.out.println ("The fitness: " + fitness);
		return fitness;
	}
	
	//This function performs reproduction and returns the new parent pool.
	public ArrayList<Chromosome> reproduction (ArrayList<Chromosome> population, int method) {
		ArrayList<Chromosome> newPopulation = new ArrayList<Chromosome>();
		Random wheel = new Random();
		double currentWheelValue;
		int size = population.size();
		
		if (method == 1) {						//Roulette selection
			double totalFitness = 0.0;
			
			for (int i = 0; i < size; i++) {
				totalFitness += population.get(i).getFitness();
			}
			for (int i = 0; i < size; i++) {
				int currFitness = population.get(i).getFitness();
				population.get(i).setInverseFitness(totalFitness/(double)currFitness);
			}
			totalFitness = 0;
			
			for (int i = 0; i < size; i++) {
				totalFitness += population.get(i).getInverseFitness();
			}
			for (int i = 0; i < size; i++) {
				population.get(i).wheelPercentage = population.get(i).getInverseFitness()/totalFitness;
				//System.out.print (population.get(i).wheelPercentage + " ");
				//System.out.println(population.get(i).getFitness());
			}
		}
		else {									//Rank selection
			double totalRankSize = 0.0;
		
			Collections.sort(population);
			for (int i = 1; i <= size; i++) {
				totalRankSize += i;
			}
			for (int i = 0; i < size; i++) {
				population.get(i).wheelPercentage = ((double)(size-i-1)) / ((double)totalRankSize);
				//System.out.println ("Rank " + (size-i-1) + ": " + population.get(i).wheelPercentage + "\tFitness: " + population.get(i).getFitness());
			}
		}
		for (int i = 0; i < size; i++) {
			currentWheelValue = wheel.nextDouble();
			double runningTotal = 0d;
			int j;
			for (j = 0; j < size; j++) {
				runningTotal += population.get(j).wheelPercentage;
				//System.out.println(runningTotal);
				if (currentWheelValue < runningTotal) break;
			}
			if (j == size) newPopulation.add(population.get(j-1));
			else newPopulation.add(population.get(j));
			//System.out.println ("Done");
		}

		/*
		System.out.println ("The old population");
		for (int i = 0; i < population.size(); i++) {
			System.out.print (population.get(i).getFitness() + " ");
			population.get(i).print();
		}
		
		System.out.println ("The reproduced parent pool");
		for (int i = 0; i < newPopulation.size(); i++) {
			System.out.print (newPopulation.get(i).getFitness() + " ");
			newPopulation.get(i).print();
		}*/
		return newPopulation;
	}
	
	public Chromosome crossover (Chromosome dominantParent, Chromosome secondaryParent, int operator) {
		if (operator == 1) {
			Chromosome child = new Chromosome(0-(dominantParent.size()));
			
			//dominantParent.print();
			//secondaryParent.print();
			//child.print();
			
			//Pick two slice points randomly.  Make sure they are not on the edges of the chromosome,
			//on top of each other, or next to each other.
			Random slicePoint = new Random();
			int slicePoint1, slicePoint2;
			do {
				slicePoint1 = slicePoint.nextInt(size);
			} while (slicePoint1 == 0);
			do {
				slicePoint2 = slicePoint.nextInt(size);
			} while (slicePoint2 == 0 || slicePoint2 == slicePoint1
					|| slicePoint2 == slicePoint1 + 1 || slicePoint2 == slicePoint1 - 1);
			
			if (slicePoint1 > slicePoint2) {
				int temp = slicePoint1;
				slicePoint1 = slicePoint2;
				slicePoint2 = temp;
			}
			
			//System.out.println ("First slice point: " + slicePoint1);
			//System.out.println ("Second slice point: " + slicePoint2);
			
			//Start putting the dominant parent's information between the slice points into the child.
			Chromosome parentCopy = new Chromosome();
			Chromosome parentSecondaryCopy = new Chromosome();
			parentCopy.copy(dominantParent);
			parentSecondaryCopy.copy(secondaryParent);
			
			//To be used in second half of algorithm.
			int origSecondarySlicePointObj = parentSecondaryCopy.getAllele(slicePoint2);
			
			int i = slicePoint1;
			for (int counter = slicePoint1; counter < slicePoint2; counter++) {
				//System.out.println(parentCopy.getAllele(i));
				child.setAllele(counter, parentCopy.remove(i));
				int numberToRemove = child.getAllele(counter);
				if (numberToRemove == origSecondarySlicePointObj) {
					int newPosition = parentSecondaryCopy.findObjectLoc (origSecondarySlicePointObj) + 1;
					if (newPosition >= parentSecondaryCopy.size()) newPosition = 0;
					origSecondarySlicePointObj = parentSecondaryCopy.getAllele(newPosition);
				}
				parentSecondaryCopy.removeObject(numberToRemove);
			}
			//Now, out of what remains, start after second slice point and cycle through, putting remaining
			//alleles into the child in the order seen in the secondary parent.
			
			//parentCopy.print();
			//parentSecondaryCopy.print();
			//System.out.print ("This is the child: ");
			//child.print();
			
			int currPosInSecondary = parentSecondaryCopy.findObjectLoc(origSecondarySlicePointObj);
			int currPosInChild = slicePoint2;
			int originalPosition = currPosInSecondary;
			int cycles = 0;
			do {
				//System.out.println ("Going to look in " + currPosInSecondary);
				//System.out.println ("Adding " + parentSecondaryCopy.getAllele(currPosInSecondary) + " to location " + currPosInChild);
				child.setAllele(currPosInChild, parentSecondaryCopy.getAllele(currPosInSecondary));
				currPosInSecondary = (currPosInSecondary + 1)%parentSecondaryCopy.size();
				currPosInChild = (currPosInChild + 1)%dominantParent.size();
				cycles++;
			} while (originalPosition != currPosInSecondary || cycles <= 1);
			
			parentCopy = null;
			parentSecondaryCopy = null;
			//System.out.print ("Final result: ");
			//child.print();
			return child;
		}
		else {
			Chromosome child = new Chromosome(0);
			child.copy(secondaryParent);
			
			//dominantParent.print();
			//secondaryParent.print();
			//child.print();
			
			ArrayList<Integer> highlightList1 = new ArrayList<Integer>();
			ArrayList<Integer> highlightList2 = new ArrayList<Integer>();
			
			//Pick four highlight spots for the dominant parent.
			Random highlightPoint = new Random();
			int newHighlight;
			do {
				boolean add = true;
				newHighlight = highlightPoint.nextInt(size);
				for (int i = 0; i < highlightList1.size(); i++) {
					if (highlightList1.get(i) == newHighlight) add = false;
				}
				if (add == true) highlightList1.add(newHighlight);
			} while (highlightList1.size() < 4);
			
			for (int i = 0; i < highlightList1.size(); i++) {
				//System.out.print (highlightList1.get(i));
			}
			//System.out.println();
			
			boolean matching = false;
			int j;
			for (int i = 0; i < highlightList1.size(); i++) {
				newHighlight = dominantParent.getAllele(highlightList1.get(i));
				j = 0;
				matching = false;
				do {
					if (secondaryParent.getAllele(j) == newHighlight) {
						//System.out.println (secondaryParent.getAllele(j) + " matches " + newHighlight);
						matching = true;
					}
					else {
						//System.out.println (secondaryParent.getAllele(j) + " does not match " + newHighlight);
						j++;
					}
				} while (matching == false);
				highlightList2.add(j);
			}
			
			/*
			for (int i = 0; i < highlightList2.size(); i++) {
				System.out.print (highlightList2.get(i));
			}
			System.out.println();*/
			
			Collections.sort(highlightList2);
			for (int i = 0; i < 4; i++) {
				//System.out.println ("Putting " + dominantParent.getAllele(highlightList1.get(i)) + " into " + highlightList2.get(i));
				child.setAllele(highlightList2.get(i), dominantParent.getAllele(highlightList1.get(i)));
			}
			//child.print();
			return child;
		}
	}
	
	public void mutation (Chromosome subject, int operator) {
		Random rnd = new Random ();
		int moveLoc = rnd.nextInt(subject.size());
		int moveDest;
		do {
		   moveDest = rnd.nextInt(subject.size());
		} while (moveDest == moveLoc);
		int moveDest2;
		do {
			moveDest2 = rnd.nextInt(subject.size());
	    } while (moveDest2 == moveLoc || moveDest2 == moveDest);
		
		//System.out.println ("I'm going to move the allele in spot " + moveLoc + " to spot " + moveDest);
		
		if (operator == 1) {					//Operator = 1, pairwise exchange
			//subject.print();
			int tempAllele = subject.getAllele(moveLoc);
			subject.setAllele(moveLoc, subject.getAllele(moveDest));
			subject.setAllele(moveDest, tempAllele);
			//subject.print();
		}
		else {	//Operator = 2, cycle of 3
			//subject.print();
			int tempAllele = subject.getAllele(moveLoc);
			subject.setAllele(moveLoc, subject.getAllele(moveDest));
			subject.setAllele(moveDest, subject.getAllele(moveDest2));
			subject.setAllele(moveDest2, tempAllele);
			//System.out.println(subject.getAllele(moveLoc) + " " + subject.getAllele(moveDest) + " " + subject.getAllele(moveDest2));
			//subject.print();
		}
	}
	
	public static void main (String [] args){
		new GeneticAlgorithm();
	}
}
